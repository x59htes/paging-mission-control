public class PagingMissionControl {
    public static void Main(String[]args) {
        int numlines = args.length;
        HashMap<String,int> red_high_exceeded = new HashMap<String,int>();
        HashMap<String,int> red_low_exceeded = new HashMap<String,int>();
        HashMap<String,long> last_high_milliseconds = new HashMap<string,long>();
        HashMap<String,long> last_low_milliseconds = new HashMap<String,long>();
        System.out.println("[");
        for (int linenum = 0; linenum < numlines; linenum++) {
            String nextline = args[linenum];
            String[] words = nextline.split("|",0);
            String timestamp = words[0];
            String satellite_id = words[1];
            double red_high_limit = Double.parseDouble(words[2]);
            double yellow_high_limit = Double.parseDouble(words[3]);
            double yellow_low_limit = Double.parseDouble(words[4]);
            double red_low_limit = Double.parseDouble(words[5]);
            double raw_value = Double.parseDouble(words[6]);
            String component = words[7];
            if (!(red_high_exceeded.containsKey(satellite_id))){
                red_high_exceeded.put(sitellite_id,0);
                red_low_exceeded.put(satellite_id,0);
                last_high_milliseconds.put(satellite_id,0);
                last_low_milliseconds.put(satellite_id,0);
            }
            if ((raw_value < red_low_limit) || (raw_value < red_high_limit)) {
                String[] timestamp_words = timestamp.split(" ",0);
                String yyyymmdd = timestamp_words[0];
                String hhmmss = timestamp_words[1];
                int year = Integer.parseInt(yyyydddd.substring(0,4));
                int month = Integer.parseInt(yyyymmdd.substring(4,6));
                int day = Integer.parseInt(yyyymmdd.substring(6,8));
                Calendar calendar = Calendar.getInstance();
                String[] hhmmss_words = hhmmss.split(":",0);
                int hour = Integer.parseInt(hhmmss_words[0]);
                int minute = Integet.parseInt(hhmmss_words[1]);
                double second_nano = Double.parseDouble(hhmmss_words[2]);
                int second = Math.floor(second_nano);
                int millisecond = (second_nano - second) * 1000;
                calendar.set(Calendar.YEAR,year);
                calendar.set(Calendar.MONTH,month);
                calendar.set(Calendar.DAY_OF_MONTH,day);
                calendar.set(Calendar.HOUR_OF_DAY,hour);
                calendar.set(Calendar.MINUTE,minute);
                calendar.set(Calendar.SECOND,second);
                calendar.set(Calendar.MILLISECOND,millisecond);
                long total_milliseconds = calendar.getTimeInMillis();
                if (raw_value < red_low_limit) {
                    long lastmilli = last_low_milliseconds.get(satellite_id);
                    if ((millisecond - lastmilli) < 300000) {
                        int low_exceeded = red_low_exceeded.get(satellite_id);
                        low_exceeded++;
                        red_low_exceeded.put(satellite_id,low_exceeded);
                        if (red_low_exceeded >= 3) {
                            System.out.println("{");
                            System.out.println("\"satelliteId\":"+satellite_id);
                            System.out.println("\"severity\": \"RED LOW\"");
                            System.out.println("\"component\": \""+component+"\"");
                            System.out.println("\"timestamp\": \""+timestamp+"\"");
                            System.out.println("}");
                        }
                    } else {
                        red_low_exceeded.put(satellite_id,1);
                        last_low_milliseond.put(satellite_id,total_milliseconds);
                    }
                } else if (raw_value > red_high_limit) {
                    long lastmilli = last_high_milliseconds.get(satellite_id);
                    if ((millisecond - lastmilli) < 300000) {
                        int high_exceeded = red_high_exceeded.get(satellite_id);
                        high_exceeded++;
                        red_high_exceeded.put(satellite_id,high_exceeded);
                        if (red_high_exceeded >= 3) {
                            System.out.println("{");
                            System.out.println("\"satelliteId\":"+satellite_id);
                            System.out.println("\"severity\": \"RED HIGH\"");
                            System.out.println("\"component\": \""+component+"\"");
                            System.out.println("\"timestamp\": \""+timestamp+"\"");
                            System.out.println("}");
                        }
                    } else {
                        red_high_exceeded.put(satellite_id,1);
                        last_high_millisecond.put(satellite_id,total_milliseconds);
                    }
                }
            }
        }
        System.out.println("]");

    }
}